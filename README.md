# OpenML dataset: Iris

https://www.openml.org/d/44344

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This is perhaps the best known database to be found in the pattern recognition literature.         Fisher's paper is a classic in the field and is referenced frequently to this day. (See Duda & Hart, for example.)         The data set contains 3 classes of 50 instances each, where each class refers to a type of iris plant.         One class is linearly separable from the other 2; the latter are NOT linearly separable from each other.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44344) of an [OpenML dataset](https://www.openml.org/d/44344). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44344/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44344/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44344/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

